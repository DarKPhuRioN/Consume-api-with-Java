package Api;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class DataModel {

    private String box, color;
    private int user;

    public DataModel(String box, String color, int user) {
        this.box = box;
        this.color = color;
        this.user = user;
    }

    public String getBox() {
        return box;
    }

    public String getColor() {
        return color;
    }

    public int getUser() {
        return user;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getDataModels() {
        return "Casilla : " + box + " Usuario : " + user + " Color : " + color;
    }

}
