package Api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class ConsumirApi {

    private String url = "";

    public ConsumirApi(String url) {
        this.url = url;
    }

    public boolean sendPostRequest(int codeprivate, int movement) {
        StringBuffer sb = null;
        try {
            String in = "";
            URL requestUrl = new URL(url + "/user/update-movement");
            String data = URLEncoder.encode("code", "UTF-8") + "=" + URLEncoder.encode(codeprivate + "", "UTF-8") + "&" + URLEncoder.encode("movimiento", "UTF-8") + "=" + URLEncoder.encode(movement + "", "UTF-8");
            HttpURLConnection conn = (HttpURLConnection) requestUrl.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(data);
            osw.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            sb = new StringBuffer();
            while ((in = br.readLine()) != null) {
                sb.append(in + "\n");
            }
            osw.close();
            br.close();
        } catch (UnsupportedEncodingException e) {// All Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (MalformedURLException e) {// All Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {// All Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String getListBoxs(int codePublic) {
        StringBuffer sb = null;
        try {
            URL requestUrl = new URL(url + "/box/table/get-table/"+codePublic);
            URLConnection conn = requestUrl.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String in = "";
            sb = new StringBuffer();
            while ((in = br.readLine()) != null) {
                sb.append(in + "\n");
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return sb.toString();
    }

}