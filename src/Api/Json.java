package Api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import Agente.Agente;
import Agente.Test;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class Json {

    private ConsumirApi app;
    private Test test;
    private int codePrivate, codePublic;
    private boolean dev;

    public Json(String url, int codePrivate, int codePublic, boolean dev) {
        this.app = new ConsumirApi(url);
        this.codePrivate = codePrivate;
        this.codePublic = codePublic;
        this.dev = dev;
        this.test = new Test(this.codePublic);
    }

    private Board getData() {
        String json1 = app.getListBoxs(this.codePublic);
        JsonParser parser = new JsonParser();
        JsonObject jsonObj = parser.parse(json1).getAsJsonObject();
        int turn = jsonObj.get("turno").getAsInt();
        if (turn > -1) {
            JsonArray demarcation = jsonObj.get("tablero").getAsJsonArray();
            DataModel[] array = new DataModel[demarcation.size()];
            int i = 0;
            for (JsonElement demarc : demarcation) {
                JsonObject ob = demarc.getAsJsonObject();
                array[i] = new DataModel(ob.get("casilla").getAsString(), ob.get("color").getAsString(), ob.get("usuario").getAsInt());
                i++;
            }
            return new Board(turn, array);
        } else {
            return new Board(turn, null);
        }
    }

    public boolean UpdateMovement(int movement) {
        return (dev) ? app.sendPostRequest(this.codePrivate, movement) : true;
    }

    public void recursive(Agente ag, int i) {
        Board table = (dev) ? this.getData() : this.test.getData(i);
        switch (table.getTurn()) {
            default:
                ag.neuralNetwork(table, this);
                recursive(ag, i + 1);
                break;
            case -1:
                ag.gameOver();
                break;
            case -3:
                ag.winner();
                break;
            case -10:
                ag.dead();
                break;
        }
    }

}
