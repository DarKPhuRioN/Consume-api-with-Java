package Api;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class Board {

    private int turn;
    private DataModel[] Board;

    public Board(int turn, DataModel[] Board) {
        this.turn = turn;
        this.Board = Board;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setDataModel(DataModel[] datamodel) {
        this.Board = datamodel;
    }

    public String getAllDataModel() {
        String datamodel = " => Turno : " + turn + " <= \n";
        if (Board != null) {
            for (int i = 0; i < Board.length; i++) {
                datamodel += Board[i].getDataModels() + "\n";
            }
            return datamodel;
        }
        return "";
    }

    public DataModel[] getPlayers() {
        return this.Board;
    }

}
