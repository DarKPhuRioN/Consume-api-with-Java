package Agente;

import Api.Json;
import Api.Board;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class Agente {

    public void neuralNetwork(Board board, Json api) {
        // board.getTurn() => Devuelve el turno
        // board.getPlayers() => Devuelve un arreglo de cada casilla
        // board.getAllDataModel() => toString Opcional del tablero
        // api.UpdateMovement(int movement) Movimiento 0 Quieto, 1 Adelante 2 Arriba 3 Abajo
        api.UpdateMovement(1);
    }

    public void gameOver() {
        System.out.println("GameOver");
    }

    public void winner() {
        System.out.println("Ganaste");
    }

    public void dead() {
        System.out.println("Muerto");
    }

}
