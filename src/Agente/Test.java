package Agente;

import Api.Board;
import Api.DataModel;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class Test {

    //-1(Alguien llego al borde), -3(Mataste 5 jugadores), -10(Estas muerto)
    private int endResponse = -1, codePublic;

    //Primer turno
    private Board firstStep(int turn) {
        DataModel[] model = {
            new DataModel("A2", "white", this.codePublic),
            new DataModel("B2", "white", 2),
            new DataModel("C2", "white", 3),
            new DataModel("D2", "white", 4),
            new DataModel("E2", "white", 5),
            new DataModel("F2", "white", 6),
            new DataModel("G2", "white", 7),
            new DataModel("H2", "white", 8),
            new DataModel("I2", "white", 9),
            new DataModel("J2", "white", 10),
            new DataModel("A9", "black", 11),
            new DataModel("B9", "black", 12),
            new DataModel("C9", "black", 13),
            new DataModel("D9", "black", 14),
            new DataModel("E9", "black", 15),
            new DataModel("F9", "black", 16),
            new DataModel("G9", "black", 17),
            new DataModel("H9", "black", 18),
            new DataModel("I9", "black", 19),
            new DataModel("J9", "black", 20)
        };
        return new Board(turn, model);
    }

    private Board twoStep(int turn) {
        DataModel[] model = {
            new DataModel("A3", "white", this.codePublic),
            new DataModel("B3", "white", 2),
            new DataModel("C3", "white", 3),
            new DataModel("D3", "white", 4),
            new DataModel("E3", "white", 5),
            new DataModel("F3", "white", 6),
            new DataModel("G3", "white", 7),
            new DataModel("H3", "white", 8),
            new DataModel("I3", "white", 9),
            new DataModel("J3", "white", 10),
            new DataModel("A8", "black", 11),
            new DataModel("B8", "black", 12),
            new DataModel("C8", "black", 13),
            new DataModel("D8", "black", 14),
            new DataModel("E8", "black", 15),
            new DataModel("F8", "black", 16),
            new DataModel("G8", "black", 17),
            new DataModel("H8", "black", 18),
            new DataModel("I8", "black", 19),
            new DataModel("J8", "black", 20)
        };
        return new Board(turn, model);
    }

    private Board threeStep(int turn) {
        DataModel[] model = {
            new DataModel("A4", "white", this.codePublic),
            new DataModel("B4", "white", 2),
            new DataModel("C4", "white", 3),
            new DataModel("D4", "white", 4),
            new DataModel("E4", "white", 5),
            new DataModel("F4", "white", 6),
            new DataModel("G4", "white", 7),
            new DataModel("H4", "white", 8),
            new DataModel("I4", "white", 9),
            new DataModel("J4", "white", 10),
            new DataModel("A7", "black", 11),
            new DataModel("B7", "black", 12),
            new DataModel("C7", "black", 13),
            new DataModel("D7", "black", 14),
            new DataModel("E7", "black", 15),
            new DataModel("F7", "black", 16),
            new DataModel("G7", "black", 17),
            new DataModel("H7", "black", 18),
            new DataModel("I7", "black", 19),
            new DataModel("J7", "black", 20)
        };

        return new Board(turn, model);
    }

    public Board getData(int turno) {
        switch (turno) {
            case 1:
                return firstStep(turno);
            case 2:
                return twoStep(turno);
            case 3:
                return threeStep(turno);
        }
        return new Board(this.endResponse, null);
    }

    public Test(int codePublic) {
        this.codePublic = codePublic;
    }

}