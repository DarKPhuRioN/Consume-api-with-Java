package Agente;

import Api.Json;

/**
 * @author Julio Alfredo Vásquez Lievano
 */
public class Main {

    public static void main(String[] args) {
        int code_public = 1, code_private = 123;
        boolean dev = false;

        Json api = new Json("http://97fb4890.ngrok.io", code_private, code_public, dev);
        api.UpdateMovement(3);//Ejecuta el default movimiento

        Agente ag = new Agente();
        api.recursive(ag, 1);
    }
}
